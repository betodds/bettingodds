﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BettingOdds.Data.Models
{
    public class Bet : IHaveIdProperty
    {
        private ICollection<Odd> odds;

        public Bet()
        {
            this.odds = new HashSet<Odd>();
        }

        [Required]
        [MaxLength(200)]
        [MinLength(1)]
        public string Name { get; set; }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        public long ID { get; set; }
        
        public bool IsLive { get; set; }
        
        public virtual ICollection<Odd> Odds
        {
            get { return this.odds; }
            set { this.odds = value; }
        }

        public long MatchID { get; set; }

        public Match Match { get; set; }

        public override bool Equals(Object obj)
        {
            var other = obj as Bet;

            if (other == null)
            {
                return false;
            }

            return this.ID == other.ID && this.IsLive == other.IsLive && this.Name == other.Name;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() >> this.Name.GetHashCode() >> this.IsLive.GetHashCode();
        }
    }
}