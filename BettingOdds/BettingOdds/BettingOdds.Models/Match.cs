﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BettingOdds.Data.Models
{
    public class Match : IHaveIdProperty
    {
        private ICollection<Bet> bets;

        public Match()
        {
            this.bets = new HashSet<Bet>();
        }

        [Required]
        [MaxLength(200)]
        [MinLength(1)]
        public string Name { get; set; }

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public long ID { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        public string MatchType { get; set; }

        public virtual ICollection<Bet> Bets
        {
            get { return this.bets; }
            set { this.bets = value; }
        }

        public long EventID { get; set; }

        public Event Event { get; set; }

        public override bool Equals(Object obj)
        {
            var other = obj as Match;

            if (other == null)
            {
                return false;
            }

            return this.ID == other.ID && 
                this.Name == other.Name &&
                this.StartDate.Equals(other.StartDate) &&
                this.MatchType == other.MatchType;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() >>
                this.Name.GetHashCode() >>
                this.StartDate.GetHashCode() <<
                (this.MatchType == null ? 0 : this.MatchType.GetHashCode());
        }
    }
}