﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BettingOdds.Data.Models
{
    public class Event : IHaveIdProperty
    {
        private ICollection<Match> matches;

        public Event()
        {
            this.matches = new HashSet<Match>();
        }

        [Required]
        [MaxLength(200)]
        [MinLength(1)]
        public string Name { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        public long ID { get; set; }

        [Required]
        public long CategoryID { get; set; }

        public bool IsLive { get; set; }

        public virtual ICollection<Match> Matches
        {
            get { return this.matches; }
            set { this.matches = value; }
        }

        public virtual Sport Sport { get; set; }

        public long SportID { get; set; }

        public override bool Equals(Object obj)
        {
            var other = obj as Event;

            if (other == null)
            {
                return false;
            }

            return this.ID == other.ID && 
                this.Name == other.Name &&
                this.IsLive == other.IsLive &&
                this.CategoryID == other.CategoryID;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() >> this.Name.GetHashCode() << this.IsLive.GetHashCode() >> this.CategoryID.GetHashCode();
        }
    }
}