﻿namespace BettingOdds.Data.Models
{
    public interface IHaveIdProperty
    {
        long ID { get; set; }
    }
}