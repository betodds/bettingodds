﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BettingOdds.Data.Models
{
    public class Odd : IHaveIdProperty
    {
        [Required]
        [MaxLength(200)]
        [MinLength(1)]
        public string Name { get; set; }

        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ID { get; set; }

        public double Value { get; set; }

        [MaxLength(20)]
        public string SpecialBetValue { get; set; }

        public long BetID { get; set; }

        public Bet Bet { get; set; }

        public override bool Equals(Object obj)
        {
            var other = obj as Odd;

            if (other == null)
            {
                return false;
            }

            return this.ID == other.ID && 
                this.Name == other.Name &&
                this.SpecialBetValue == other.SpecialBetValue &&
                this.Value == other.Value;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() >> 
                this.Name.GetHashCode() <<
                this.Value.GetHashCode() >>
                (this.SpecialBetValue == null ? 0 : this.SpecialBetValue.GetHashCode());
        }
    }
}