﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BettingOdds.Data.Models
{
    public class Sport : IHaveIdProperty
    {
        private ICollection<Event> events;

        public Sport()
        {
            this.events = new HashSet<Event>();
        }

        [Required]
        [MaxLength(200)]
        [MinLength(1)]
        [Index]
        public string Name { get; set; }

        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ID { get; set; }

        public virtual ICollection<Event> Events
        {
            get { return this.events; }
            set { this.events = value; }
        }

        public override bool Equals(object obj)
        {
            var other = (obj as Sport);

            if(other == null)
            {
                return false;
            }

            return this.ID == other.ID && this.Name == other.Name;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() >> this.Name.GetHashCode();
        }
    }
}