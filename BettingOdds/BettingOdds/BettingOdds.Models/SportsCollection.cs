﻿using System.Collections.Generic;

namespace BettingOdds.Data.Models
{
    public class SportsCollection
    {
        public SportsCollection()
        {
            this.Sports = new HashSet<Sport>();
        }
        
        public ICollection<Sport> Sports { get; set; }
    }
}
