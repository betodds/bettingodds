﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

using BettingOdds.Data;
using BettingOdds.Data.Models;

namespace BettingOdds.DbUpdater
{
    public class BettingOddsCache
    {
        public BettingOddsCache(IBettingOddsDbContext context)
        {
            this.OddsIDs = new ConcurrentDictionary<long, Odd>(context.Odds.ToDictionary(o => o.ID, o => o));
            this.SportsIDs = new ConcurrentDictionary<long, Sport>(context.Sports.ToDictionary(s => s.ID, s => s));
            this.MatchesIDs = new ConcurrentDictionary<long, Match>(context.Matches.ToDictionary(m => m.ID, m => m));
            this.BetsIDs = new ConcurrentDictionary<long, Bet>(context.Bets.ToDictionary(b => b.ID, b => b));
            this.EventsIDs = new ConcurrentDictionary<long, Event>(context.Events.ToDictionary(e => e.ID, e => e));
        }

        public IDictionary<long, Odd> OddsIDs { get; private set; }

        public IDictionary<long, Sport> SportsIDs { get; set; }

        public IDictionary<long, Match> MatchesIDs { get; set; }

        public IDictionary<long, Event> EventsIDs { get; set; }

        public IDictionary<long, Bet> BetsIDs { get; set; }

        public IDictionary<long, T> GetProperty<T>()
        {
            var property = this.GetType().GetProperties()
                .FirstOrDefault(p => p.PropertyType.Equals(typeof(IDictionary<long, T>)));

            if (property == null)
            {
                throw new ArgumentException("There is no property of type: " + typeof(T).ToString());
            }

            return property.GetValue(this) as IDictionary<long, T>;
        }
    }
}
