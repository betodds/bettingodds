﻿namespace BettingOdds.DbUpdater
{
    public class InsertUpdateDataHolder
    {
        public InsertUpdateDataHolder()
        {
            this.ElementsToUpdate = new BettingOddsDataHolder();
            this.ElementsToInsert = new BettingOddsDataHolder();
        }

        public BettingOddsDataHolder ElementsToInsert { get; private set; }

        public BettingOddsDataHolder ElementsToUpdate { get; private set; }
    }
}
