﻿using System;
using System.Linq;
using System.Collections.Generic;

using BettingOdds.Data.Models;

namespace BettingOdds.DbUpdater
{
    public class BettingOddsDataHolder
    {
        public BettingOddsDataHolder()
        {
            this.Odds = new HashSet<Odd>();
            this.Bets = new HashSet<Bet>();
            this.Sports = new HashSet<Sport>();
            this.Matches = new HashSet<Match>();
            this.Events = new HashSet<Event>();
        }

        public ISet<Odd> Odds { get; private set; }

        public ISet<Sport> Sports { get; set; }

        public ISet<Match> Matches { get; set; }

        public ISet<Event> Events { get; set; }

        public ISet<Bet> Bets { get; set; }

        public ISet<T> GetProperty<T>()
        {
            var property = this.GetType().GetProperties()
                .FirstOrDefault(p => p.PropertyType.Equals(typeof(ISet<T>)));

            if (property == null)
            {
                throw new ArgumentException("There is no property of type: " + typeof(T).ToString());
            }

            return property.GetValue(this) as ISet<T>;
        }
    }
}
