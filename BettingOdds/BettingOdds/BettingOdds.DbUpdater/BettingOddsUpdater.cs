﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;

using BettingOdds.Data;
using BettingOdds.Data.Models;
using EntityFramework.BulkExtensions.Operations;

namespace BettingOdds.DbUpdater
{
    public class BettingOddsUpdater<SType>
    {
        private const string InsertedDataFormat = "{0} data: \n sports: {1} \n events: {2} \n matches: {3} \n bets: {4} \n odds: {5}";

        private TextWriter output;
        private BettingOddsCache data;

        public BettingOddsUpdater(BettingOddsCache data, TextWriter output)
            : this(data)
        {
            this.output = output;
        }

        public BettingOddsUpdater(BettingOddsCache data)
        {
            this.data = data;
        }
        
        public virtual void UpadateAsync(SportsCollection data)
        {
            var dataHolder = new InsertUpdateDataHolder();

            WriteToOutput("\nUpdate Start       " + DateTime.Now.ToString("hh:mm:ss - yyyy-MM-dd"));

            SeparateExistingData(data, dataHolder);
            
            BulkInsertData(dataHolder.ElementsToInsert);
            LogInformation(dataHolder.ElementsToInsert, "Inserted");

            UpdateData(dataHolder.ElementsToUpdate);
            LogInformation(dataHolder.ElementsToUpdate, "Updated");

            WriteToOutput("Update Finish      " + DateTime.Now.ToString("hh:mm:ss - yyyy-MM-dd"));
        }

        private void SeparateExistingData(SportsCollection data, InsertUpdateDataHolder dataHolder)
        {
            foreach (var sport in data.Sports)
            {
                this.AddOrUpdate(sport, dataHolder.ElementsToInsert.Sports, dataHolder.ElementsToUpdate.Sports);

                foreach (var @event in sport.Events)
                {
                    @event.Sport = sport;
                    @event.SportID = sport.ID;

                    this.AddOrUpdate(@event, dataHolder.ElementsToInsert.Events, dataHolder.ElementsToUpdate.Events);

                    foreach (var match in @event.Matches)
                    {
                        match.Event = @event;
                        match.EventID = @event.ID;

                        this.AddOrUpdate(match, dataHolder.ElementsToInsert.Matches, dataHolder.ElementsToUpdate.Matches);

                        foreach (var bet in match.Bets)
                        {
                            bet.Match = match;
                            bet.MatchID = match.ID;

                            this.AddOrUpdate(bet, dataHolder.ElementsToInsert.Bets, dataHolder.ElementsToUpdate.Bets);

                            foreach (var odd in bet.Odds)
                            {
                                odd.Bet = bet;
                                odd.BetID = bet.ID;

                                this.AddOrUpdate(odd, dataHolder.ElementsToInsert.Odds, dataHolder.ElementsToUpdate.Odds);
                            }
                        }
                    }
                }
            }
        }

        private void UpdateData(BettingOddsDataHolder dataHolder)
        {
            using (var dbset = new BettingOddsDbContext())
            {
                //Update the entities
                dbset.BulkUpdate(dataHolder.Odds);
                dbset.BulkUpdate(dataHolder.Bets);
                dbset.BulkUpdate(dataHolder.Matches);
                dbset.BulkUpdate(dataHolder.Events);
                dbset.BulkUpdate(dataHolder.Sports);

                dbset.SaveChanges();
            }
        }

        private static void BulkInsertData(BettingOddsDataHolder dataHolder)
        {
            using (var dbset = new BettingOddsDbContext())
            {
                dbset.BulkInsert(dataHolder.Odds);
                dbset.BulkInsert(dataHolder.Bets);
                dbset.BulkInsert(dataHolder.Matches);
                dbset.BulkInsert(dataHolder.Events);
                dbset.BulkInsert(dataHolder.Sports);

                dbset.SaveChanges();
            }            
        }

        protected void Update<T>(BettingOddsDataHolder dataHolder, IDbSet<T> dbSet)
            where T : class
        {
            foreach (var entity in dataHolder.GetProperty<T>())
            {
                dbSet.AddOrUpdate(entity);
            }
        }

        protected void AddOrUpdate<T>(T value, ISet<T> collectionForInsert, ISet<T> collectionForUpdate)
            where T : IHaveIdProperty
        {
            var cachedValues = this.data.GetProperty<T>();

            var isExists = cachedValues.ContainsKey(value.ID);

            // contains the element and values are not equal (updated)
            if (isExists && !cachedValues[value.ID].Equals(value))
            {
                collectionForUpdate.Add(value);
                cachedValues[value.ID] = value;
            }
            else if (!isExists)
            {
                cachedValues.Add(value.ID, value);
                collectionForInsert.Add(value);
            }
        }

        protected void LogInformation(BettingOddsDataHolder dataHolder, string operationType)
        {
            this.WriteToOutput(string.Format(InsertedDataFormat,
                                operationType,
                                dataHolder.Sports.Count,
                                dataHolder.Events.Count,
                                dataHolder.Matches.Count,
                                dataHolder.Bets.Count,
                                dataHolder.Odds.Count));
        }

        protected void WriteToOutput(string message)
        {
            if (this.output != null)
            {
                output.WriteLine(message);
            }
        }
    }
}
