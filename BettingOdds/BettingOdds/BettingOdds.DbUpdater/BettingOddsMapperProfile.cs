﻿using AutoMapper;

using BettingOdds.Data.Models;
using BettingOdds.Requester.Models;

namespace BettingOdds.DbUpdater
{
    public class BettingOddsMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<XmlSportsModel, SportsCollection>();
            CreateMap<XmlSport, Sport>();
            CreateMap<XmlEvent, Event>();
            CreateMap<XmlMatch, Match>();
            CreateMap<XmlBet, Bet>();
            CreateMap<XmlOdd, Odd>();
        }
    }
}
