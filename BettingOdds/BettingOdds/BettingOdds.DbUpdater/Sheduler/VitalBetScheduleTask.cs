﻿using System;
using System.Timers;
using System.Xml.Serialization;

using AutoMapper;

using BettingOdds.Data.Models;
using BettingOdds.Requester;
using BettingOdds.Requester.Models;


namespace BettingOdds.DbUpdater
{
    public class VitalBetScheduleTask : IScheduleTask
    {
        private BettingOddsUpdater<XmlSportsModel> updater;

        public VitalBetScheduleTask(BettingOddsUpdater<XmlSportsModel> updater)
        {
            this.updater = updater;
        }

        public async void UpdateOdds(object sender, ElapsedEventArgs e)
        {
            try
            {
                var requester = new ApiRequester("http://vitalbet.net/", "sportxml", DataType.Xml);
                var xml = await requester.Request();
                var serializer = new XmlSerializer(typeof(XmlSportsModel));
                
                var xmlData = (XmlSportsModel)serializer.Deserialize(xml);

                var data = Mapper.Map<SportsCollection>(xmlData);

                this.updater.UpadateAsync(data);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Fail updating the DB", ex);
            }
        }
    }
}