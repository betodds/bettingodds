﻿using AutoMapper;
using System.Timers;

namespace BettingOdds.DbUpdater
{
    public class Scheduler
    {
        private Timer timer;

        public Scheduler(double interval = 60000)
        {
            Mapper.Initialize(profilers =>
            {
                profilers.AddProfile<BettingOddsMapperProfile>();
            });

            this.timer = new Timer();
            this.timer.Interval = interval;
            this.timer.AutoReset = true;
        }

        public void Stop()
        {
            this.timer.Stop();
        }

        public void BeginUpdate()
        {
            this.timer.Start();
        }

        public void AddUpdateMethod(ElapsedEventHandler eventHandler)
        {
            this.timer.Elapsed += eventHandler;
        }
    }
}
