﻿using System.Timers;

namespace BettingOdds.DbUpdater
{
    public interface IScheduleTask
    {
        void UpdateOdds(object sender, ElapsedEventArgs e);
    }
}