﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BettingOdds.Requester.Models
{
    public class XmlBet
    {
        public XmlBet()
        {
            this.Odds = new List<XmlOdd>();
        }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("ID")]
        public long ID { get; set; }

        [XmlAttribute("IsLive")]
        public bool IsLive { get; set; }

        [XmlElement("Odd")]
        public List<XmlOdd> Odds { get; set; }
    }
}