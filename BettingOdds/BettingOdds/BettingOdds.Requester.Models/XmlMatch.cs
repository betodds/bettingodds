﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BettingOdds.Requester.Models
{
    public class XmlMatch
    {
        public XmlMatch()
        {
            this.Bets = new List<XmlBet>();
        }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("ID")]
        public long ID { get; set; }

        [XmlAttribute("StartDate")]
        public DateTime StartDate { get; set; }

        [XmlAttribute("MatchType")]
        public string MatchType { get; set; }

        [XmlElement("Bet")]
        public List<XmlBet> Bets { get; set; }
    }
}