﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BettingOdds.Requester.Models
{
    [XmlRoot(elementName: "XmlSports")]
    public class XmlSportsModel
    {
        public XmlSportsModel()
        {
            this.Sports = new List<XmlSport>();
        }

        [XmlElement("Sport")]
        public List<XmlSport> Sports { get; set; }
    }
}
