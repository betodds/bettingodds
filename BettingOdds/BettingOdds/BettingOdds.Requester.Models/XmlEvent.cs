﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BettingOdds.Requester.Models
{
    public class XmlEvent
    {
        public XmlEvent()
        {
            this.Matches = new List<XmlMatch>();
        }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("ID")]
        public long ID { get; set; }

        [XmlAttribute("CategoryID")]
        public long CategoryID { get; set; }

        [XmlAttribute("IsLive")]
        public bool IsLive { get; set; }

        [XmlElement("Match")]
        public List<XmlMatch> Matches { get; set; }
    }
}