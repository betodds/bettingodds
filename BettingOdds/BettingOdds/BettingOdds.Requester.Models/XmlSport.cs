﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BettingOdds.Requester.Models
{
    public class XmlSport
    {
        public XmlSport()
        {
            this.Events = new List<XmlEvent>();
        }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("ID")]
        public long ID { get; set; }

        [XmlElement("Event")]
        public List<XmlEvent> Events { get; set; }
    }
}