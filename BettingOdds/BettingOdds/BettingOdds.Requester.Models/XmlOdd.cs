﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace BettingOdds.Requester.Models
{
    public class XmlOdd
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("ID")]
        public long ID { get; set; }

        [XmlAttribute("Value")]
        public double Value { get; set; }

        [XmlAttribute("SpecialBetValue")]
        public string SpecialBetValue { get; set; }
    }
}