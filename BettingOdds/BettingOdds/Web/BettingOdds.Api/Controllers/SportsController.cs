﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

using AutoMapper;

using BettingOdds.Api.Models;
using BettingOdds.Data.Models;
using BettingOdds.Service.Sports;
using AutoMapper.QueryableExtensions;

namespace BettingOdds.Api.Controllers
{
    public class SportsController : ApiController
    {
        private ISportsService sportsService;

        public SportsController(ISportsService sportsService)
        {
            this.sportsService = sportsService;
        }

        // GET api/sports
        public async Task<IHttpActionResult> Get()
        {
            var sports = await this.sportsService.AllSports()
                .ProjectTo<SportViewModel>()
                .ToListAsync();

            return Json(sports);
        }
    }
}
