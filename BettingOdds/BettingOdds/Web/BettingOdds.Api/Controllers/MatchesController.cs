﻿using System.Linq;
using System.Web.Http;

using AutoMapper.QueryableExtensions;

using BettingOdds.Service.Matches;
using System;
using BettingOdds.Api.Models;
using System.Data.Entity;
using System.Threading.Tasks;
using BettingOdds.Api.Common;

namespace BettingOdds.Api.Controllers
{
    public class MatchesController : ApiController
    {
        private const int MatchesToTake = 6;

        private IMatchesCacheingProvider matchesCache;
        private IMatchesService matchesService;

        public MatchesController(IMatchesService matchesService, IMatchesCacheingProvider matchesCache)
        {
            this.matchesService = matchesService;
            this.matchesCache = matchesCache;
        }

        // GET api/matches/id
        [Route("api/matches/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(long id)
        {
            var matches = await this.matchesService.GetMatches(id, DateTime.Now)
                    .ProjectTo<MatchViewModel>()
                    .ToListAsync();

            return Ok(matches);
        }

        [Route("api/matches/upcomming/{page}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(int page)
        {
            if (page <= 0)
            {
                return BadRequest("Pages cannot be less or equal to 0!");
            }

            var matches = (await this.matchesService.GetUpcommingMaches()
                    .ProjectTo<MatchViewModel>()
                    .ToListAsync())
                    .Skip((page - 1) * MatchesToTake)
                    .Take(MatchesToTake);

            return Ok(matches);
        }

        [Route("api/matches/updated/{pages}")]
        [HttpGet]
        public IHttpActionResult GetUpdated(int pages)
        {
            var matches = this.matchesService.GetUpcommingMaches()
                    .ProjectTo<MatchViewModel>()
                    .ToList()
                    .Take(pages * MatchesToTake);

            var updated = this.matchesCache.GetUpdatedMatches(matches);

            return Ok(updated);
        }
    }
}
