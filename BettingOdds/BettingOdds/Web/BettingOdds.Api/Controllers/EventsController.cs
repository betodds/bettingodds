﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;

using AutoMapper.QueryableExtensions;

using BettingOdds.Api.Models;
using BettingOdds.Service.Events;

namespace BettingOdds.Api.Controllers
{
    public class EventsController : ApiController
    {
        private IEventsService eventsService;

        public EventsController(IEventsService eventsService)
        {
            this.eventsService = eventsService;
        }


        // GET api/events/5
        [HttpGet]
        public async Task<IHttpActionResult> Get(long id)
        {
            var events = await this.eventsService
                .GetEvents(id)
                .ProjectTo<EventViewModel>()
                .ToListAsync();

            return Ok(events);
        }
    }
}
