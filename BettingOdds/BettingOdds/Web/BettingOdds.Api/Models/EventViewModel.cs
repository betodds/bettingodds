﻿namespace BettingOdds.Api.Models
{
    public class EventViewModel
    {
        public long ID { get; set; }
        
        public bool IsLive { get; set; }

        public string Name { get; set; }
    }
}