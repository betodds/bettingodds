﻿namespace BettingOdds.Api.Models
{
    public class OddViewModel
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public string SpecialBetValue { get; set; }

        public string Value { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as OddViewModel;

            if(other == null)
            {
                return false;
            }

            return this.ID == other.ID &&
                this.Name == other.Name &&
                this.SpecialBetValue == other.SpecialBetValue &&
                this.Value == other.Value;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() >>
                this.Name.GetHashCode() <<
                (this.Value == null ? 0 : this.Value.GetHashCode()) >>
                (this.SpecialBetValue == null ? 0 : this.SpecialBetValue.GetHashCode());
        }
    }
}