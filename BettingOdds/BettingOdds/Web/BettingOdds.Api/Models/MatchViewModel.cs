﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BettingOdds.Api.Models
{
    public class MatchViewModel
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public ICollection<BetViewModel> Bets { get; set; }

        public string SportName { get; set; }

        public string EventName { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as MatchViewModel;

            if(other == null)
            {
                return false;
            }

                return this.ID == other.ID &&
                this.Name == other.Name &&
                this.StartDate == other.StartDate &&
                this.Bets.Except(other.Bets).Count() == 0;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() >> this.StartDate.GetHashCode() >> this.Name.GetHashCode();
        }
    }
}