﻿using System.Collections.Generic;
using System.Linq;

namespace BettingOdds.Api.Models
{
    public class BetViewModel
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public bool IsLive { get; set; }

        public ICollection<OddViewModel> Odds { get; set; }


        public override bool Equals(object obj)
        {
            var other = obj as BetViewModel;

            if(other == null)
            {
                return false;
            }

            return this.ID == other.ID &&
                this.Name == other.Name &&
                this.IsLive == other.IsLive &&
                this.Odds.Except(other.Odds).Count() == 0;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() << this.Name.GetHashCode() >> this.IsLive.GetHashCode();
        }
    }
}