[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(BettingOdds.Api.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(BettingOdds.Api.App_Start.NinjectWebCommon), "Stop")]

namespace BettingOdds.Api.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    using Data;

    using Service.Sports;
    using Service.Events;
    using Service.Matches;
    using Common;
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static IKernel Kernel { get; private set; }

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            Kernel = kernel;

            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterDependenceies(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterDependenceies(IKernel kernel)
        {
            RegisterDbContexts(kernel);
            
            RegisterRepositories(kernel);

            RegisterServices(kernel);

            kernel.Bind<IMatchesCacheingProvider>().To<MatchesCacheingProvider>().InSingletonScope();
        }

        /// <summary>
        /// Register all cervices needed for the controllers
        /// </summary>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ISportsService>().To<SportsService>();
            kernel.Bind<IEventsService>().To<EventsService>();
            kernel.Bind<IMatchesService>().To<MatchesService>();
        }
        
        /// <summary>
        /// Register all repositories
        /// </summary>
        private static void RegisterRepositories(IKernel kernel)
        {
            kernel.Bind(typeof(IRepository<>)).To(typeof(EfGenericRepository<>));
        }

        /// <summary>
        /// Register all DB contexts
        /// </summary>
        private static void RegisterDbContexts(IKernel kernel)
        {
            kernel.Bind<IBettingOddsDbContext>().To<BettingOddsDbContext>().InRequestScope();
        }
    }
}
