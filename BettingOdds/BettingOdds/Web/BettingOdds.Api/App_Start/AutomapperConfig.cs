﻿using AutoMapper;

using BettingOdds.Data.Models;
using BettingOdds.Api.Models;

namespace BettingOdds.Api
{
    public class AutomapperConfig : Profile
    {
        protected override void Configure()
        {
            CreateMap<Sport, SportViewModel>()
                .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            CreateMap<Event, EventViewModel>()
               .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.IsLive, opt => opt.MapFrom(src => src.IsLive));

            CreateMap<Odd, OddViewModel>()
               .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value))
               .ForMember(dest => dest.SpecialBetValue, opt => opt.MapFrom(src => src.SpecialBetValue));

            CreateMap<Bet, BetViewModel>()
               .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.IsLive, opt => opt.MapFrom(src => src.IsLive))
               .ForMember(dest => dest.Odds, opt => opt.MapFrom(src => src.Odds));

            CreateMap<Match, MatchViewModel>()
               .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
               .ForMember(dest => dest.SportName, opt => opt.MapFrom(src => src.Event.Sport.Name))
               .ForMember(dest => dest.EventName, opt => opt.MapFrom(src => src.Event.Name))
               .ForMember(dest => dest.Bets, opt => opt.MapFrom(src => src.Bets));            
        }
    }
}