﻿using BettingOdds.Api.Models;
using Ninject.Infrastructure.Language;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BettingOdds.Api.Common
{
    public class MatchesCacheingProvider : IMatchesCacheingProvider
    {
        private IDictionary<long, MatchViewModel> matchesCache;

        public MatchesCacheingProvider()
        {
            //TODO: Clean the cache on some period
            this.matchesCache = new ConcurrentDictionary<long, MatchViewModel>();
        }

        public IEnumerable<MatchViewModel> GetUpdatedMatches(IEnumerable<MatchViewModel> matches)
        {
            var updatedMatches = new List<MatchViewModel>();

            foreach (var match in matches)
            {
                var exists = this.matchesCache.ContainsKey(match.ID);

                if (exists && !match.Equals(this.matchesCache[match.ID]))
                {
                    updatedMatches.Add(match);
                    this.matchesCache[match.ID] = match;
                }
                else if (!exists)
                {
                    this.matchesCache.Add(match.ID, match);
                    updatedMatches.Add(match);
                }
            }


            return updatedMatches;
        }
    }
}