﻿using System.Collections.Generic;

using BettingOdds.Api.Models;

namespace BettingOdds.Api.Common
{
    public interface IMatchesCacheingProvider
    {
        IEnumerable<MatchViewModel> GetUpdatedMatches(IEnumerable<MatchViewModel> matches);
    }
}