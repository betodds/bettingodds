﻿using AutoMapper;

using System.Web.Http;
using System.Web.Mvc;

namespace BettingOdds.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);

            Mapper.Initialize(profilers =>
            {
                profilers.AddProfile<AutomapperConfig>();
            });
        }
    }
}
