﻿using BettingOdds.Data;
using BettingOdds.Data.Migrations;
using BettingOdds.DbUpdater;
using BettingOdds.Requester.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BettingOdds.DbUpdater.ConsoleApp
{
    public class StartUp
    {
        //TODO: Handle usage from machines and add ninject or similar (if possible)
        public static void Main()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BettingOddsDbContext, Configuration>());

            var shedule = new Scheduler();

            try
            {

                using (var dbContext = new BettingOddsDbContext())
                {
                    var dbUpdater = new BettingOddsUpdater<XmlSportsModel>(new BettingOddsCache(dbContext), Console.Out);
                    var sheduleTask = new VitalBetScheduleTask(dbUpdater);

                    shedule.AddUpdateMethod(sheduleTask.UpdateOdds);
                    shedule.BeginUpdate(); 
                }
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex);
            }
            catch(Exception)
            {
                Console.WriteLine("Updating fail!");

                shedule.Stop();
            }

            Console.WriteLine("Press 'q' to stop updating!");
            while (true)
            {
                var key = Console.ReadKey();

                if(key.KeyChar == 'q')
                {
                    break;
                }
            }

            Console.WriteLine("Thank you!");
        }
    }
}
