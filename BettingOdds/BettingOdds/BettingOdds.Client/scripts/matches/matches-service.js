﻿(function () {

    function MatchesService($http, apiUrl) {

        function getMatches(eventId) {
            return $http.get(apiUrl + '/api/matches/' + eventId);
        }

        function getUpcomming(page) {
            return $http.get(apiUrl + '/api/matches/upcomming/' + page);
        }

        function getUpdatedData(pages) {
            return $http.get(apiUrl + '/api/matches/updated/' + pages);
        }

        return {
            get: getMatches,
            getUpcomming: getUpcomming,
            getUpdatedData: getUpdatedData
        }
    }

    angular.module('bettingOdds.services')
        .factory('MatchesService', ['$http', 'apiUrl', MatchesService]);
}())