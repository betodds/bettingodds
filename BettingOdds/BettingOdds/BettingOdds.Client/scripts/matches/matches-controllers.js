﻿(function () {

    function MatchesController($scope, $sessionStorage, MatchesService, Notification) {

        MatchesService.get($sessionStorage.currentEventID)
            .success(function (data) {
                $scope.matches = data;
                $scope.title = "Follow odds for betts"
            })
            .error(function () {
                $scope.title = "Fail load matches, please try again!";
                Notification.error("Sorry cannot load matches for this sport!")
            })
    }

    angular.module('bettingOdds.controllers')
        .controller('MatchesController', ['$scope', '$sessionStorage', 'MatchesService', 'Notification', MatchesController]);
}())