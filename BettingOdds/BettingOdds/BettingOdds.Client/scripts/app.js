﻿(function () {
    'use strict';

    function config($routeProvider, $locationProvider) {

        $locationProvider.html5Mode(true);

        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'scripts/home/home.html'
            })
            .when('/event/:sportId', {
                templateUrl: 'scripts/events/events.html',
                controller: 'EventsController'
            })
            .when('/match/:eventId', {
                templateUrl: 'scripts/matches/matches.html',
                controller: 'MatchesController'
            })
            .otherwise({ redirectTo: '/' });
        //.when('/event/matches', {

        //})
        //.when('/event/match/bets', {

        //})
        //.when('/event/match/bet/odds', {

        //})
        //.otherwise({ redirectTo: '/' });
    }


    angular.module('bettingOdds.services', ['bettingOdds']);

    angular.module('bettingOdds.controllers', ['bettingOdds.services', 'bettingOdds.notification']);

    angular.module('bettingOdds.notification', ['ui-notification'])
         .config(function (NotificationProvider) {
             NotificationProvider.setOptions({
                 delay: 1000,
                 startTop: 20,
                 startRight: 10,
                 verticalSpacing: 20,
                 horizontalSpacing: 20,
                 positionX: 'center',
                 positionY: 'top'
             });
         });

    angular.module('bettingOdds', ['ngRoute', 'ngStorage', 'bettingOdds.controllers'])
        .config(['$routeProvider', '$locationProvider', config])
        .constant('apiUrl', 'http://localhost:5918');
}());