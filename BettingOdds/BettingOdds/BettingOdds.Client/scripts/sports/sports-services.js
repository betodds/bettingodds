﻿(function () {
    function SportsService($http, apiUrl) {
        
        function _getSports() {
            return $http.get(apiUrl + '/api/sports');
        }

        return {
            get: _getSports
        };
    }

    angular.module('bettingOdds.services')
        .factory('SportsService', ['$http', 'apiUrl', SportsService]);
}());