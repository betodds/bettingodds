﻿(function () {

    function SportsController($scope, $sessionStorage, SportsService, Notification) {
        
        SportsService.get()
            .success(function (data) {
                $scope.sports = data;
                Notification.success({ message: 'Welcome to Betting Odds!' });
            });

        $scope.setCurrentSport = function setCurrentSport(id) {
            $sessionStorage.currentSportID = id;
        }
    }

    angular.module('bettingOdds.controllers')
        .controller('SportsController', ['$scope', '$sessionStorage', 'SportsService', 'Notification', SportsController]);
}());