﻿(function () {

    function HomeController($scope, $interval, MatchesService) {
        var pageCounter = 1;

        $scope.loadMore = function () {
            MatchesService.getUpcomming(pageCounter++)
            .success(function (data) {
                angular.forEach(data, function (match) {
                    $scope.matches.push(match);
                })
            })
            .error(function () {
                Notification.error({ message: 'Cannot load more!' });
            });
        }

        $interval(function () {
            MatchesService.getUpdatedData(pageCounter)
                .success(function (data) {
               
                    angular.forEach(data, function (updatedMatch) {
                        for (var i = 0; i < $scope.matches.length; i++) {
                            var match = $scope.matches[i];

                            if (updatedMatch.ID == match.ID && !angular.equals(updatedMatch, match)) {
                                console.log($scope.matches[i]);
                                $scope.matches[i] = updatedMatch;
                                console.log(updatedMatch);
                                break;

                            }
                        }
                    });
                });
        }, 6000);
        
        MatchesService.getUpcomming(pageCounter++)
            .success(function (data) {
                $scope.matches = data;

            })
            .error(function () {
                Notification.error({ message: 'Cannot get upcomming matches from the server!' });
            });
    }

    angular.module('bettingOdds.controllers')
        .controller('HomeController', ['$scope', '$interval', 'MatchesService', HomeController]);
}());