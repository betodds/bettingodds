﻿(function () {

    function EventsService($http, apiUrl) {

        function _getEvents(sportID) {
            return $http.get(apiUrl + '/api/events/' + sportID);
        }

        return {
            get: _getEvents
        }
    }

    angular.module('bettingOdds.services')
        .factory('EventsService', ['$http', 'apiUrl', EventsService]);
}());