﻿(function () {
    function EventsController($scope, $sessionStorage, EventsService, Notification) {
        
        EventsService.get($sessionStorage.currentSportID)
            .success(function (data) {
                $scope.events = data;
            })
            .error(function () {
                Notification.error({ message: 'Cannot get events from the server!' });
            })

        $scope.setCurrentEventID = function (eventID) {
            $sessionStorage.currentEventID = eventID;
        }
    }

    angular.module('bettingOdds.controllers')
        .controller('EventsController', ['$scope', '$sessionStorage', 'EventsService', 'Notification', EventsController])
}());