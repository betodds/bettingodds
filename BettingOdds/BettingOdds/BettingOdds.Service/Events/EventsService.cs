﻿using System.Linq;

using BettingOdds.Data;
using BettingOdds.Data.Models;

namespace BettingOdds.Service.Events
{
    public class EventsService : IEventsService
    {
        private IRepository<Event> eventsRepo;

        public EventsService(IRepository<Event> eventsRepo)
        {
            this.eventsRepo = eventsRepo;
        }

        public IQueryable<Event> GetEvents(long sportId)
        {
            return this.eventsRepo.All()
                .Where(e => e.SportID == sportId);
        }
    }
}
