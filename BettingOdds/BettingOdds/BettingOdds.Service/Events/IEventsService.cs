﻿using System.Linq;

using BettingOdds.Data.Models;

namespace BettingOdds.Service.Events
{
    public interface IEventsService
    {
        IQueryable<Event> GetEvents(long sportId);
    }
}
