﻿using System.Linq;

using BettingOdds.Data.Models;
using BettingOdds.Data;

namespace BettingOdds.Service.Sports
{
    public class SportsService : ISportsService
    {
        private IRepository<Sport> sportsRepo;

        public SportsService(IRepository<Sport> sportsRepo)
        {
            this.sportsRepo = sportsRepo;
        }

        public IQueryable<Sport> AllSports()
        {
            return this.sportsRepo.All();
        }
    }
}
