﻿using System.Linq;

using BettingOdds.Data.Models;

namespace BettingOdds.Service.Sports
{
    public interface ISportsService
    {
        IQueryable<Sport> AllSports();
    }
}