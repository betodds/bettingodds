﻿using System;
using System.Linq;

using BettingOdds.Data.Models;

namespace BettingOdds.Service.Matches
{
    public interface IMatchesService
    {
        IQueryable<Match> GetMatches(long eventId, DateTime startDate);

        IQueryable<Match> GetUpcommingMaches();
    }
}
