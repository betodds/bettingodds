﻿using System;
using System.Linq;

using BettingOdds.Data;
using BettingOdds.Data.Models;

namespace BettingOdds.Service.Matches
{
    public class MatchesService : IMatchesService
    {
        private IRepository<Match> matchesRepo;

        public MatchesService(IRepository<Match> matchesRepo)
        {
            this.matchesRepo = matchesRepo;
        }

        public IQueryable<Match> GetMatches(long eventId, DateTime startDate)
        {
            return this.matchesRepo.All()
                .Where(m => m.EventID == eventId && m.StartDate >= startDate);
        }

        public IQueryable<Match> GetUpcommingMaches()
        {
            var startDate = DateTime.Now;
            var endDate = startDate.AddDays(24);

            return this.matchesRepo.All()
                .Where(m => m.StartDate >= startDate && m.StartDate <= endDate && m.Bets.Count() > 0)
                .OrderBy(m => m.Event.Sport.Name);

        }
    }
}
