﻿using System.IO;
using System.Threading.Tasks;

namespace BettingOdds.Requester
{
    public interface IRequester
    {
        Task<Stream> Request();
    }
}