﻿using System;

namespace BettingOdds.Requester
{
    public static class DataTypeExtentions
    {
        public static string AsString(this DataType type)
        {
            switch (type)
            {
                case DataType.Xml:
                    return "application/xml";
                case DataType.JSON:
                    return "application/json";
                default:
                    throw new ArgumentException("There is such data type " + type.ToString());
            }
        }
    }
}
