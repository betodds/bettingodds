﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BettingOdds.Requester
{
    public class ApiRequester : IRequester
    {
        public ApiRequester(string url, string requestUri, DataType type)
        {
            this.Url = url;
            this.RequestUri = requestUri;
            this.DataType = type;
        }

        public DataType DataType { get; private set; }

        public string RequestUri { get; private set; }

        public string Url { get; private set; }

        public async Task<Stream> Request()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(this.Url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(this.DataType.AsString()));

                HttpResponseMessage response = await client.GetAsync(this.RequestUri);

                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        return await response.Content.ReadAsStreamAsync();
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException("Fail reading respond content!", ex);
                    }
                }
                else
                {
                    throw new ArgumentException("Error getting the information!");
                }
            }
        }
    }
}
