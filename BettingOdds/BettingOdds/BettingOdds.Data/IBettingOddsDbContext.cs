﻿using System.Data.Entity;

using BettingOdds.Data.Models;
using System.Data.Entity.Infrastructure;

namespace BettingOdds.Data
{
    public interface IBettingOddsDbContext
    {
        IDbSet<Sport> Sports { get; set; }

        IDbSet<Odd> Odds { get; set; }

        IDbSet<Bet> Bets { get; set; }

        IDbSet<Match> Matches { get; set; }

        IDbSet<Event> Events { get; set; }

            DbSet<TEntity> Set<TEntity>() where TEntity : class;

            DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

            void Dispose();

            int SaveChanges();
    }
}
