﻿using System.Data.Entity;

using BettingOdds.Data.Models;

namespace BettingOdds.Data
{
    public class BettingOddsDbContext : DbContext, IBettingOddsDbContext
    {
        public BettingOddsDbContext() 
            : base("BettingOdds")
        {
        }

        public virtual IDbSet<Sport> Sports { get; set; }

        public virtual IDbSet<Odd> Odds { get; set; }

        public virtual IDbSet<Bet> Bets { get; set; }

        public virtual IDbSet<Match> Matches { get; set; }

        public virtual IDbSet<Event> Events { get; set; }
    }
}
